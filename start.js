const express = require("express");
const app = express();
const posts = [
  { id: 1, title: "POST 1" },
  { id: 2, title: "POST 2" },
  { id: 3, title: "POST 3" },
  { id: 4, title: "POST 4" },
];
app.get("/", (req, res) => {
  res.send(`You gonna be alright...! `);
});

app.get("/posts", (req, res) => {
  res.json(posts);
});

app.get('*', (err, req, res, next) => {
  console.error(err.stack);
  res.send("Something broke!");
});

app.listen(3000, () => {
  console.log("running on the backend @ localhost:3000...!");
});
